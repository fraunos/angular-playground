import { Component, OnInit } from '@angular/core';
import { TrelloBBService } from './trello-bb.service';

import { Bubble } from './bubble/bubble';
import { Thread } from './bubble/thread/thread';

@Component({
  selector: 'app-trello-bb',
  templateUrl: './trello-bb.component.html',
  styleUrls: ['./trello-bb.component.css']
})
export class TrelloBBComponent implements OnInit {
  exampleData
  bubbles: Bubble[]
  newBubbleName: string = ''

  constructor(private service: TrelloBBService) { }

  ngOnInit() {
    this.getBubbles()
  }

  private getBubbles(): void {
    this.service.getLists().subscribe(data => this.bubbles = this.parseListsToBubbles(data))
  }
  private createNewList(): void {
    this.service.createNewList(this.newBubbleName).subscribe(data => this.getBubbles())
    this.newBubbleName = ''
  }

  private parseListsToBubbles(lists): Bubble[] {
    lists = lists.sort((a, b) => { return a.name > b.name })

    let bubbles: Bubble[] = []
    for (let list of lists) {
      let parent = bubbles.find(item => this.checkIfNested(item, list))

      if (parent) {
        while (parent && parent.bubbles.length > 0) {
          let tmp = parent.bubbles.find(item => this.checkIfNested(item, list))
          if (!tmp) break;
          parent = tmp
        }
        parent.bubbles.push(new Bubble(list))
      } else {
        bubbles.push(new Bubble(list))
      }
    }
    return bubbles
  }
  private checkIfNested(parent, child): boolean {
    let parentLayers = parent.name.split(':')
    let childLayers = child.name.split(':')
    let tmp = childLayers.findIndex(item => item === parentLayers[parentLayers.length - 1])

    return tmp !== -1
  }
}
