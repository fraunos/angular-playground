import { TestBed, inject } from '@angular/core/testing';

import { TrelloBBService } from './trello-bb.service';

describe('TrelloBBService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TrelloBBService]
    });
  });

  it('should be created', inject([TrelloBBService], (service: TrelloBBService) => {
    expect(service).toBeTruthy();
  }));
});
