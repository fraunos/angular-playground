import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrelloBBComponent } from './trello-bb.component';

describe('TrelloBBComponent', () => {
  let component: TrelloBBComponent;
  let fixture: ComponentFixture<TrelloBBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrelloBBComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrelloBBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
