import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { TrelloBBComponent } from './trello-bb.component';
// import { TrelloBBService } from './trello-bb.service';
import { BubbleComponent } from './bubble/bubble.component';
import { FormsModule } from '@angular/forms';
import { ThreadComponent } from './bubble/thread/thread.component';
import { PostComponent } from './bubble/thread/post/post.component';

@NgModule({
  declarations: [
    TrelloBBComponent,
    BubbleComponent,
    ThreadComponent,
    PostComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
  ],
  // providers: [ TrelloBBService ]
})
export class TrelloBBModule { }
