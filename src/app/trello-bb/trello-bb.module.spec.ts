import { TrelloBBModule } from './trello-bb.module';

describe('TrelloBBModule', () => {
  let trelloBBModule: TrelloBBModule;

  beforeEach(() => {
    trelloBBModule = new TrelloBBModule();
  });

  it('should create an instance', () => {
    expect(trelloBBModule).toBeTruthy();
  });
});
