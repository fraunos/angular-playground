import { Component, OnInit, Input } from '@angular/core';
import { TrelloBBService } from '../../trello-bb.service';

import { Thread } from './thread'
import { Post } from './post/post'

@Component({
  selector: 'app-thread',
  templateUrl: './thread.component.html',
  styleUrls: ['./thread.component.css']
})
export class ThreadComponent implements OnInit {
  @Input() thread: Thread;
  showContent: boolean = false
  newPost: string = ''

  constructor(private service: TrelloBBService) { }

  ngOnInit() {
    this.getPosts()
  }
  getPosts() {
    this.service.getPosts(this.thread.id).subscribe(data => {
      let posts = []
      for (let post of data) {
        posts.push(new Post(post))
      }
      posts.sort((a, b) => { return a.date - b.date })
      this.thread.posts = posts
    })
  }
  createNewPost() {
    this.service.createNewPost(this.thread.id, this.newPost).subscribe(data => this.getPosts())
    this.newPost = ''
  }
  deleteThread() {
    this.service.deleteThread(this.thread.id).subscribe(data => this.thread = data)
  }
}
