export class Post {
  id: string;
  author: string;
  text: string;
  date: Date;

  constructor(obj){
    this.id = obj.id
    this.text = obj.data.text
    this.date = new Date(obj.date)
  }
}
