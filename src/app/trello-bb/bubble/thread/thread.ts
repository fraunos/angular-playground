import { Post } from './post/post';

export class Thread {
  id: string;
  name: string;
  closed: boolean;
  desc: string;
  posts: Post[] = [];

  constructor(obj) {
    this.id = obj.id
    this.name = obj.name
    this.closed = obj.closed
  }
}
