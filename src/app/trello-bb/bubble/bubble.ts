import { Thread } from './thread/thread';

export class Bubble {
  id: string;
  name: string;
  closed: boolean;

  threads: Thread[] = [];
  bubbles: Bubble[] = [];

  constructor(obj) {
    this.id = obj.id
    this.name = obj.name
    this.closed = obj.closed
  }
}
