import { Component, OnInit, Input } from '@angular/core';
import { TrelloBBService } from '../trello-bb.service';

import { Bubble } from './bubble';
import { Thread } from './thread/thread';

@Component({
  selector: 'app-bubble',
  templateUrl: './bubble.component.html',
  styleUrls: ['./bubble.component.css']
})
export class BubbleComponent implements OnInit {
  @Input() bubble: Bubble;
  showContent: boolean = false
  newThreadName: string = ''

  constructor(private service: TrelloBBService) { }

  ngOnInit() {
    this.getThreads()
  }

  deleteBubble() {
    this.service.deleteList(this.bubble.id).subscribe(data => this.bubble = data)
  }
  showContents() {
    this.showContent = !this.showContent
  }
  getThreads() {
    this.service.getThreads(this.bubble.id).subscribe(data => {
      let threads = []
      for (let thread of data) {
          threads.push(new Thread(thread))
      }
      this.bubble.threads = threads
    })
  }
  createNewThread() {
    this.service.createNewThread(this.bubble.id, this.newThreadName).subscribe(data => this.getThreads())
    this.newThreadName = ''
  }
}
