import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

import { Bubble } from './bubble/bubble';
import { Thread } from './bubble/thread/thread';
import { Post } from './bubble/thread/post/post';

@Injectable({
  providedIn: 'root'
})
export class TrelloBBService {
  trelloBoard = 'qMKmMa08';
  trelloBoardId = '5b0bfd910883473310258313';
  trelloAPIKey = 'cb55d042c8c02a3a6feb36e95cde20fe';
  trelloToken = 'f25f9a9bba8bd73aacf707def82e433225b872a092a3a0f0778247d479645872';
  trelloAPI = 'https://api.trello.com/1';
  trelloAuth = `key=${this.trelloAPIKey}&token=${this.trelloToken}`

  constructor(private http: HttpClient) { }

  getLists() {
    return this.http.get<Bubble[]>(`${this.trelloAPI}/boards/${this.trelloBoardId}/lists?${this.trelloAuth}`)
  }
  createNewList(name) {
    return this.http.post<Bubble>(`${this.trelloAPI}/lists?name=${name}&idBoard=${this.trelloBoardId}&${this.trelloAuth}`, null)
    // .pipe(
    //   catchError(this.handleError('addList'))
    // );
  }
  deleteList(id) {
    return this.http.put<Bubble>(`${this.trelloAPI}/lists/${id}/closed?value=true&${this.trelloAuth}`, null)
  }
  getThreads(id): Observable<Thread[]> {
    return this.http.get<Thread[]>(`${this.trelloAPI}/lists/${id}/cards?${this.trelloAuth}`)
  }

  getPosts(id): Observable<Post[]> {
    return this.http.get<Post[]>(`${this.trelloAPI}/cards/${id}/actions?filter=commentCard&${this.trelloAuth}`)
  }
  createNewPost(id, text) {
    return this.http.post<Thread>(`${this.trelloAPI}/cards/${id}/actions/comments?text=${text}&${this.trelloAuth}`, null)
  }
  createNewThread(id, name) {
    return this.http.post<Thread>(`${this.trelloAPI}/cards?idList=${id}&name=${name}&${this.trelloAuth}`, null)
  }
  deleteThread(id) {
    return this.http.put<Thread>(`${this.trelloAPI}/cards/${id}/?closed=true&${this.trelloAuth}`, null)
  }



  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
}
