import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'

import { TrelloBBComponent } from './trello-bb/trello-bb.component';

const appRoutes: Routes = [
  { path: 'trelloBB', component: TrelloBBComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class AppRoutingModule { }
